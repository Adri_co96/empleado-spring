package com.ejemplo.controller;

import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ejemplo.service.IEmpleado;

@Produces({"application/json"})
@RestController
@RequestMapping("/proyecto")
public class EmpleadoController {
	
	@Autowired
	private IEmpleado empleado;
	

	@GetMapping(path ="/saludar/{nombre}")
	public String holaMundo(@PathVariable ("nombre") String nombre) {
		String respuesta=null;
		
		try {
			respuesta = empleado.holaMundo(nombre);
			
		}
		catch (Exception ex){
			System.out.print("Error: "+ex.getStackTrace());
		}
		
		
		return respuesta;
		
		
	}
	
	
	

}
