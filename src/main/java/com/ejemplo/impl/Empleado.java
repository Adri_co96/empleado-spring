/**
 * 
 */
package com.ejemplo.impl;

import org.springframework.stereotype.Service;

import com.ejemplo.service.IEmpleado;

/**
 * 
 */
@Service
public class Empleado implements IEmpleado {

	@Override
	public String holaMundo(String nombre) {
		//Va la logica de negocio
		return "hola mundo "+nombre;
	}
	
	

}
